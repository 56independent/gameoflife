import java.util.Arrays;

public class GolPorter {
    Info info = new Info();

    public static Info encode(String scheme, String text, Info info){
        /*
         * Turns an encoded GOL string of x scheme into a intermediate representation class called Info for usage with the convert method.
         */

        boolean nameNow = false;
        int rowIndex = 0;
        int index = 0;

        info.cells = new int[text.length()][text.length()];

        for (int i = 0; i < text.length(); i++) {
            Character currentChar = text.charAt(i);

            if (scheme.equals("plaintext")) {
                boolean restOfLineIsNotCells = false;
                String buffer = "";

                if (currentChar == '\n') {
                    rowIndex++;
                    index = 0;

                    if (nameNow) {
                        info.name = buffer;
                    } else if (restOfLineIsNotCells) {
                        info.description = buffer;
                    }
                } else if (currentChar == '!') {
                    restOfLineIsNotCells = true;
                } else if (currentChar == 'O' || currentChar == '.' && !restOfLineIsNotCells) {
                    info.cells[index][rowIndex] = (currentChar == 'O') ? 1 : 0;
                    index++;
                } else {
                    if (buffer.toLowerCase() == "name:") {
                        nameNow = true;
                    }
                }
            } else if (scheme.equals("Xes")) {
                if (currentChar == '\n') {
                    rowIndex++;
                    index = 0;
                } else {
                    info.cells[rowIndex][index] = (currentChar.toString().toLowerCase() == "x") ? 1 : 0;
                    index++;
                }
            }
        }
        return info;
    }

    public static String decode(String scheme, int[][] cells, Info info) {

        String decoded = "";

        for (int i = 0; i < cells.length; i++){
            for (int o = 0; o < cells[i].length; o++){
                int currentState = cells[i][o];
                boolean alive = (currentState == 1) ? true : false;

                if (scheme.toLowerCase() == "xes"){
                    decoded += (alive) ? "X" : " ";
                }
            }

            decoded += "\n";
        }

        return decoded;
    }


    public String textToScheme(String originalScheme, String endScheme, String textToChange) {
        return decode(endScheme, encode(originalScheme, textToChange, info).cells, info);
    }
}
