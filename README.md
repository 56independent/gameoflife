# Game Of Life
## How to use
Copy and paste the code into Processing IDE. Run it.

To change the state of a cell, left-click.

By using `o`, one can open a [plaintext](https://conwaylife.com/patterns/gosperglidergun.cells) file and  import it. By using `w`, one can then write the current state to the file.

Using `+` and `-`, one can zoom in and out. To move around the cell field, you may use the arrow keys.

Planned features:

* `ctrl` for selection of cells
* `c` to copy
* `v` to paste
* Soup Creation faciltiies.

Beware: 100% of users (just me at the moment) have reported temporary blurring of vision that relieves quickly after looking at the program; be careful!

## Credits
All code and commits created by 56independent, some debugging work and tedious-to-write code written by ChatGPT.

## License
MIT
