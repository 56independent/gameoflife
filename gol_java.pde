import javax.swing.JOptionPane; // For error message
import java.io.*;
import java.util.Random;
import java.awt.event.KeyEvent;

int size = 200; // The length of the square array of cells.
int cutOffValue = 90; // Random number from 1 to 100 being higher produces a live cell at this value
int cellsize = 5; // How big the cells should be

int[] offsets = new int[2]; // How much to offset the cells

int[][] agarCells = new int[size][size]; // This is our matrix of cells. The cells within are defined as Binary numbers, where 1 stands for a living cell, and 0 for a dead cell.

boolean mouseStillPressed = false;
boolean doStep = false;
boolean openFile = false;
boolean writeFile = false;
boolean soupGeneration = false;


/*
for (i = 0; i < agarCells.length(); i++){
  java.util.Arrays.fill(agarCells, 0);
}
*/

void setup() {
  fullScreen();
}

void draw() {
  if (mousePressed != true){
    mouseStillPressed = false;
  } else {
    mouseStillPressed = true;
  }
  
  background(0);
  stroke(255);

  if (keyPressed) {
      if (keyCode == UP) {
        offsets[1] += cellsize;
      } else if (keyCode == DOWN) {
        offsets[1] -= cellsize;
      } else if (keyCode == LEFT) {
        offsets[0] += cellsize;
      } else if (keyCode == RIGHT) {
        offsets[0] -= cellsize;
      }
    }

  for (int i = 0; i < size; i++){
    for (int o = 0; o < size; o++){
      // Define the region of the screen that the mouse must be within to return true
      int minX = (i * cellsize) + offsets[0];
      int minY = (o * cellsize) + offsets[1];
      int maxX = minX + cellsize;
      int maxY = minY + cellsize;
        
      // Check if the mouse is within the defined region
      boolean isMouseWithinRegion = (mouseX >= minX && mouseX < maxX && mouseY >= minY && mouseY < maxY) ? true : false;
        
      // Combine the above conditions to determine if the mouse is currently within the region and has just been pressed
      boolean mouseHere = mousePressed && isMouseWithinRegion;
        
      if (mouseHere) {
        mouseStillPressed = true;
        if (agarCells[i][o] == 1){
          agarCells[i][o] = 0;
        } else {
          agarCells[i][o] = 1;
        }
      }
      
      if (agarCells[i][o] == 1) {
        stroke(0);
        fill(255);
      } else {
        stroke(255);
        fill(0);
      }
      
      rect((i * cellsize) + offsets[0], (o * cellsize) + offsets[1], cellsize, cellsize);
    }
  }

  if (doStep){
    //JOptionPane.showMessageDialog(null,"Due to financial constraints, you must apply the GOL rules yourself.","Budget Cuts",1);

    int[][] transientAgar = new int[size][size];

    /*
    Map of co-ordinates for better conceptualising map indexing:

    (-1, +1), (+0, +1), (+1, +1)
    (-1, +0), (+0, +0), (+1, +0)
    (-1, -1), (+0, -1), (+1, -1)
    */

    for (int i = 0; i < size; i++){
      for (int o = 0; o < size; o++){
        int neighbourCount = 0;

        // Now introducing: The WETTEST code you have ever seen! It's so innovative and wet it makes the dry towel put water into the code!
        // Code Maintainers: Please don your find and replace, Regexes, and low spirits before entering this level 7 restricted zone

        neighbourCount += ((i < size-1) && (o < size-1)) ? agarCells[i+1][o+1] : 0;
        neighbourCount += (              (o < size-1  )) ? agarCells[ i ][o+1] : 0;
        neighbourCount += ((i > 0   ) && (o < size-1  )) ? agarCells[i-1][o+1] : 0;
        neighbourCount += ((i < size-1)                ) ? agarCells[i+1][ o ] : 0;
      //neighbourCount += (                            ) ? agarCells[ i ][ o ] : 0; // Middle cell; ignore.
        neighbourCount += ((i > 0   )                  ) ? agarCells[i-1][ o ] : 0;
        neighbourCount += ((i < size-1) && (o > 0     )) ? agarCells[i+1][o-1] : 0;
        neighbourCount += (              (o > 0       )) ? agarCells[ i ][o-1] : 0;
        neighbourCount += ((i > 1   ) && (o > 0       )) ? agarCells[i-1][o-1] : 0;

        if ((neighbourCount == 2 || neighbourCount == 3) && agarCells[i][o] == 1){
          transientAgar[i][o] = 1;
        } else if (neighbourCount == 3 && agarCells[i][o] == 0){
          transientAgar[i][o] = 1;
        } else {
          transientAgar[i][o] = 0;
        }
      }
    }

    agarCells = transientAgar;
    doStep = false;
  } if (openFile) {
    openFile = false;
    selectInput("Select a plaintext GOL file to import:", "fileNowSelected");
  } if (writeFile) {
    writeFile = false;
    selectOutput("Select a plaintext GOL file to export to:", "fileNowSelectedToWrite");
  } if (soupGeneration) {
    soupGeneration = false;

    Random rand = new Random();

    for (int i = 0; i < size; i++){
      for (int o = 0; o < size; o++){
        agarCells[i][o] = (rand.nextInt(100) > cutOffValue-1) ? 1 : 0;
      }
    }
  }
}

void fileNowSelected(File selection) {
  Info info = new Info();

  try {
    BufferedReader reader = new BufferedReader(new FileReader(selection));
    String line;
    StringBuilder stringBuilder = new StringBuilder();
    while ((line = reader.readLine()) != null) {
      stringBuilder.append(line);
      stringBuilder.append(System.lineSeparator());
    }
    reader.close();
    String contents = stringBuilder.toString();
    info = GolPorter.encode("plaintext", contents, info);

    size = info.cells.length;

    agarCells = info.cells;
  } catch (FileNotFoundException e) {
    JOptionPane.showMessageDialog(null,"File not is, so file not able to open...????","File nope",1);
  } catch (IOException e) {
    JOptionPane.showMessageDialog(null,"IO exception what happened why u wrong","File nope",1);
  }
}

void fileNowSelectedToWrite(File selection) {
  Info info = new Info();

  try {
    String contents = GolPorter.decode("plaintext", agarCells, info);

    BufferedWriter writer = new BufferedWriter(new FileWriter(selection));
    writer.write(contents);
    writer.close();
  } catch (FileNotFoundException e) {
    JOptionPane.showMessageDialog(null,"File not is, so file not able to open...????","File nope",1);
  } catch (IOException e) {
    JOptionPane.showMessageDialog(null,"IO exception what happened why u wrong","File nope",1);
  }
}


void keyPressed(){
  if (key == ' '){
    doStep = true;
  } else if (key == 'o'){
    openFile = true;
  } else if (key == 'w'){
    writeFile = true;
  } else if (key == 's'){
    soupGeneration = true;
  } else if (key == '+'){
    cellsize++;
  } else if (key == '-'){
    cellsize--;
  } else {
    offsets[0] = (key == KeyEvent.VK_LEFT) ? offsets[0]-- : (key == KeyEvent.VK_RIGHT) ? offsets[0] : offsets[0];
  }
}

// Writing copyright to me, some debugging by ChatGPT
