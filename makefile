compileLibs:
	javac Info.java
	javac GolPorter.java

compileProgram: compileLibs
	rm -rf /tmp/processing
	mkdir /tmp/processing
	java -cp .:/path/to/processing-core.jar GolPorter /tmp/processing/

testLibs: compileLibs
	java tester.java

testProcessing: compileProgram