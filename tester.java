public class tester {
    public static void main(String[] args){
        GolPorter gol = new GolPorter();

        String testPlaintextString = 
        "!Name: myfile\n" +
        ".O.OOOO..... \n" +
        ".O.OOOO....O \n";
        
        Info info = new Info();

        gol.encode("plaintext", testPlaintextString, info);

        System.out.println(gol.textToScheme("plaintext", "xes", testPlaintextString));
    }
}
